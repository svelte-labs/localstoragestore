import { fromLocalStorage } from './lib/store/localStorage'

export const count = fromLocalStorage(
  'localStorageCounter', 0,
  val => val.toString(), val => parseInt(val)
)