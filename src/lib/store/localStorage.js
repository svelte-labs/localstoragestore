// for some inspiration check: https://github.com/sveltejs/svelte/blob/master/src/runtime/store/index.ts#L64

import { writable } from 'svelte/store'

const fromLocalStorage = (localStorageKey, defaultValue, toStringFn, fromStringFn) => {
  if (!localStorageKey) throw new Error('No localStorage key specified')
  if (typeof localStorageKey !== 'string') throw new Error('localStorage key should be a string')

  // assign default serialize/unserialize functions
  if (!toStringFn) toStringFn = value => value.toString()
  if (!fromStringFn) fromStringFn = value => value

  // private variable to hold the value
  let _value = null

  let initValue = localStorage.getItem(localStorageKey)

  // read _value from data persisted in localStorage
  if (initValue !== null) {
    _value = fromStringFn(initValue)
  // save defaultValue in localStorage
  } else if (initValue === null && (defaultValue !== null && defaultValue !== undefined)) {
    _value = defaultValue
    localStorage.setItem(localStorageKey, toStringFn(_value))
  }

  // initialize private store
  const _store = writable(_value)

  const set = value => {
    _value = value    // private value
    localStorage.setItem(localStorageKey, toStringFn(_value)) // localStorage
    _store.set(_value) // store
  }

  const update = (fn) => set(fn(_value))

  const subscribe = _store.subscribe

  return { set, update, subscribe }
}

export { fromLocalStorage }